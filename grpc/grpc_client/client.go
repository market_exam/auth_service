package grpc_client

import (
	"app/genproto/user_service"
	"fmt"

	"google.golang.org/grpc"

	"app/config"
)

type GrpcClientI interface {
	UserService() user_service.UserServiceClient
}

type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

func New(cfg config.Config) (*GrpcClient, error) {
	connUser, err := grpc.Dial(
		fmt.Sprintf("%s:%s", cfg.UserServiceHost, cfg.UserServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("user service dial host: %s port: %s",
			cfg.UserServiceHost, cfg.UserServicePort)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"user_service": user_service.NewUserServiceClient(connUser),
		},
	}, nil
}

func (g *GrpcClient) UserService() user_service.UserServiceClient {
	return g.connections["user_service"].(user_service.UserServiceClient)
}
