package grpc

import (
	"app/genproto/auth_service"

	"app/config"
	"app/grpc/grpc_client"
	"app/grpc/service"
	"app/pkg/logger"
	"app/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.Logger, strg storage.StorageI, svcs grpc_client.GrpcClientI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()
	auth_service.RegisterAuthServiceServer(grpcServer, service.NewAuthService(cfg, strg, log, svcs))
	reflection.Register(grpcServer)
	return
}
