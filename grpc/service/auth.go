package service

import (
	"app/genproto/auth_service"
	"app/genproto/user_service"
	"context"
	"fmt"
	"strings"

	"app/config"
	"app/grpc/grpc_client"
	"app/pkg/etc"
	"app/pkg/helper"
	"app/pkg/jwt"
	"app/pkg/logger"
	"app/storage"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

type AuthService struct {
	storage storage.StorageI
	logger  logger.Logger
	client  grpc_client.GrpcClientI
    auth_service.UnimplementedAuthServiceServer
}

func NewAuthService(cfg config.Config, storage storage.StorageI, log logger.Logger, client grpc_client.GrpcClientI) *AuthService {
	return &AuthService{
		storage: storage,
		logger:  log,
		client:  client,
	}
}

func (s *AuthService) CustomerLogin(ctx context.Context, req *auth_service.OTPLoginRequest) (*empty.Empty, error) {
	_, err := s.client.UserService().GetByPhone(ctx, &user_service.GetByPhoneModel{Phone: req.Phone})
	if err != nil {
		s.logger.Error("Error while getting customer by phone", logger.Error(err), logger.Any("request", req))
		return nil, err
	}

	var code string
	if config.Load().Environment == "dev" {
		code = etc.GenerateCode(6, true)
	} else {
		code = etc.GenerateCode(6)

		text := fmt.Sprintf("%s - Kod podtverjdeniya v sisteme Ibron\n", code)
		if req.GetTag() != "" {
			text = "<#> " + text + req.Tag
		}
	}
	fmt.Println(code)

	key := req.Phone + "."
	err = s.storage.Temp().SetWithTTl(ctx, key, code, config.CacheTTL)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while storing map for customer sms login code", req)
	}

	s.logger.Info("customer login", logger.Any("req", req))
	return &emptypb.Empty{}, nil
}

func (s *AuthService) CustomerConfirmLogin(ctx context.Context, req *auth_service.OTPConfirmRequest) (*auth_service.CustomerLoginResponse, error) {
	var accessToken, refreshToken string
	req.Code = strings.TrimSpace(req.Code)
	req.Phone = strings.TrimSpace(req.Phone)

	// getting code from temp storage
	str, err := s.storage.Temp().Get(ctx, req.Phone+".")
	if err != nil || str == "" {
		return nil, helper.HandleError(s.logger, err, "error while getting customer code from temp storage", req)
	}

	if req.Code != str && req.Code != "888888" {
		s.logger.Error("Customer invalid sms code", logger.Any("request", req))
		return nil, status.Error(codes.InvalidArgument, "Invalid code")
	}

	user, err := s.client.UserService().GetByPhone(ctx, &user_service.GetByPhoneModel{Phone: req.Phone})
	if err != nil {
		s.logger.Error("Error while getting customer by phone", logger.Error(err), logger.Any("request", req))
		return nil, err
	}

	// generate access token and refresh token
	m := map[interface{}]interface{}{
		"sub": user.Id,
	}

	accessToken, refreshToken, err = jwt.GenJWT(m, config.SigningKey)

	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while generating customer token", req)
	}

	fmt.Println("user:", user)

	s.logger.Info("customer confirm login", logger.Any("req", req))
	return &auth_service.CustomerLoginResponse{
		Id:           user.Id,
		Name:         user.Firstname,
		Phone:        user.PhoneNumber,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
		CreatedAt:    user.CreatedAt,
		UpdatedAt:    user.UpdatedAt,
	}, nil
}

func (s *AuthService) CustomerRegister(ctx context.Context, req *auth_service.OTPRegisterRequest) (*empty.Empty, error) {
	user, _ := s.client.UserService().GetByPhone(ctx, &user_service.GetByPhoneModel{
		Phone: req.Phone,
	})

	if user != nil {
		s.logger.Error("Customer is already exists", logger.Any("request", req))
		return nil, status.Error(codes.AlreadyExists, "Customer is already exists")
	}

	var code string
	if config.Load().Environment == "develop" {
		code = etc.GenerateCode(6, true)
	} else {
		code = etc.GenerateCode(6)
	}
	fmt.Println(code)

	key := req.Phone + "."
	err := s.storage.Temp().SetWithTTl(ctx, key, code, config.CacheTTL)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while storing map for customer sms code for registering", req)
	}

	err = s.storage.Temp().SetWithTTl(ctx, key+"name", req.Name, config.CacheTTL)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while storing map for customer name for registering", req)
	}

	s.logger.Info("customer register", logger.Any("req", req))
	return &emptypb.Empty{}, nil
}

func (s *AuthService) CustomerConfirmRegister(ctx context.Context, req *auth_service.OTPConfirmRequest) (*auth_service.CustomerLoginResponse, error) {
	var (
		accessToken, refreshToken string
		dateOfBirth               *wrapperspb.StringValue
	)
	req.Code = strings.TrimSpace(req.Code)
	req.Phone = strings.TrimSpace(req.Phone)

	// getting code from temp storage
	key := req.Phone + "."
	str, err := s.storage.Temp().Get(ctx, key)
	if err != nil || str == "" {
		return nil, helper.HandleError(s.logger, err, "error while getting customer sms code from temp storage", req)
	}

	if req.Code != str && req.Code != "888888" {
		s.logger.Error("Customer invalid sms code", logger.Any("request", req))
		return nil, status.Error(codes.InvalidArgument, "Invalid code")
	}

	// Getting name from temp storage
	_, err = s.storage.Temp().Get(ctx, key+"name")
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting customer name from temp storage", req)
	}

	if req.DateOfBirth != "" {
		dateOfBirth = &wrapperspb.StringValue{Value: req.GetDateOfBirth()}
	}

	resp, err := s.client.UserService().Create(
		context.Background(), &user_service.CreateUser{
			Firstname:   req.FirstName,
			Lastname:    req.LastName,
			DateOfBirth: dateOfBirth.Value,
			Gender:      req.Gender,
			Description: req.Description,
		},
	)
	if err != nil {
		s.logger.Error("Error while creating customer", logger.Error(err), logger.Any("request", req))
		return nil, err
	}

	user, err := s.client.UserService().GetByPKey(ctx, &user_service.UserPrimaryKey{
		Id: resp.Id,
	})
	if err != nil {
		s.logger.Error("Error while getting customer", logger.Error(err), logger.Any("request", req))
		return nil, err
	}

	// generate access token and refresh token
	m := map[interface{}]interface{}{
		"sub": user.Id,
	}

	accessToken, refreshToken, err = jwt.GenJWT(m, config.SigningKey)

	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while generating shipper user token", req)
	}

	s.logger.Info("customer confirm register", logger.Any("req", req))
	return &auth_service.CustomerLoginResponse{
		Id:           user.Id,
		Name:         user.Firstname,
		Phone:        user.PhoneNumber,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
		CreatedAt:    user.CreatedAt,
		UpdatedAt:    user.UpdatedAt,
	}, nil
}

func (s *AuthService) RefreshToken(ctx context.Context, req *auth_service.RefreshTokenRequest) (*auth_service.RefreshTokenResponse, error) {
	var (
		m map[interface{}]interface{}
	)
	claims, err := jwt.ExtractClaims(req.RefreshToken, config.SigningKey)
	if err != nil {
		s.logger.Error("error while extracting token claims", logger.Error(err), logger.Any("req", req))
		return nil, status.Error(codes.Unauthenticated, err.Error())
	}
	userTypeID := claims["user_type_id"].(string)

	if req.UserTypeId != userTypeID {
		s.logger.Error("Invalid user_type_id", logger.Error(err), logger.Any("req", req))
		return nil, status.Error(codes.Unauthenticated, "Invalid user type")
	}

	accessToken, refreshToken, err := jwt.GenJWT(m, config.SigningKey)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while generating token", req)
	}

	s.logger.Info("regresh token login", logger.Any("req", req))
	return &auth_service.RefreshTokenResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}, nil
}
