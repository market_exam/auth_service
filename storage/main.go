package storage

import (
	"context"
)

type StorageI interface {
	CloseDB()
	Temp() TempStorageI
}

type TempStorageI interface {
	SetWithTTl(ctx context.Context, key, value string, interval string) error
	Get(ctx context.Context, key string) (string, error)
}
