package postgres

import (
	"context"
	"fmt"

	"app/config"
	"app/pkg/logger"
	"app/storage"

	"github.com/jackc/pgx/v5/pgxpool"
)

type Store struct {
	db   *pgxpool.Pool
	log  logger.Logger
	temp storage.TempStorageI
}

func NewPostgres(ctx context.Context, cfg config.Config, log logger.Logger) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s?sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	))
	if err != nil {
		return nil, err
	}

	config.MaxConns = int32(cfg.MaxConnections)

	pool, err := pgxpool.NewWithConfig(ctx, config)
	if err != nil {
		return nil, err
	}
	err = pool.Ping(ctx)
	if err != nil {
		return nil, err
	}

	return &Store{
		db:  pool,
		log: log,
	}, err
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Temp() storage.TempStorageI {
	if s.temp == nil {
		s.temp = NewTempRepo(s.db)
	}
	return s.temp
}
