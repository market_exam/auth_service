package postgres

import (
	"context"

	"app/storage"

	"github.com/jackc/pgx/v5/pgxpool"
)

type tempRepo struct {
	db *pgxpool.Pool
}

func NewTempRepo(db *pgxpool.Pool) storage.TempStorageI {
	return &tempRepo{db: db}
}

func (c *tempRepo) SetWithTTl(ctx context.Context, key, value string, interval string) error {
	_, err := c.db.Exec(ctx, `
		INSERT INTO temp(key, value, expiry_interval) 
			VALUES ($1, $2, $3)
	`, key, value, interval)

	if err != nil {
		return err
	}

	return nil
}

func (c *tempRepo) Get(ctx context.Context, key string) (string, error) {
	var code string

	query := c.db.QueryRow(ctx, `
		SELECT value FROM temp WHERE key=$1 AND created_at > NOW() - expiry_interval ORDER BY created_at DESC
	`, key)

	err := query.Scan(&code)
	if err != nil {
		return "", err
	}

	return code, nil
}
