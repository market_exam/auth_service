drop trigger if exists temp_delete_old_rows_trigger on temp;
drop function if exists temp_delete_old_rows();
drop table if exists temp;