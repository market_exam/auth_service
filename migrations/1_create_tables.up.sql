
CREATE TABLE IF NOT EXISTS temp (
    key varchar,
    value varchar,
    created_at timestamp default NOW(),
    expiry_interval INTERVAL
);

CREATE FUNCTION temp_delete_old_rows() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM temp WHERE created_at < NOW() -  expiry_interval;
  RETURN NEW;
END;
$$;

CREATE TRIGGER temp_delete_old_rows_trigger
    AFTER INSERT ON temp
    EXECUTE PROCEDURE temp_delete_old_rows();

