package helper

import (
	"strconv"
	"strings"

	"app/pkg/logger"

	"github.com/gocql/gocql"
	"github.com/gomodule/redigo/redis"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func HandleError(log logger.Logger, err error, msg string, req interface{}) error {
	if err == gocql.ErrNotFound || err == redis.ErrNil {
		log.Error(msg+", not found", logger.Any("request", req))
		return status.Error(codes.NotFound, "not found")
	} else if err != nil {
		log.Error(msg, logger.Error(err), logger.Any("request", req))
		return status.Error(codes.Internal, "internal server error")
	}

	return nil
}

func ReplaceSQL(old, searchPattern string) string {
	tmpCount := strings.Count(old, searchPattern)
	for m := 1; m <= tmpCount; m++ {
		old = strings.Replace(old, searchPattern, "$"+strconv.Itoa(m), 1)
	}
	return old
}
