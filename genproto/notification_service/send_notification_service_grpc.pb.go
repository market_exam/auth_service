// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.12
// source: send_notification_service.proto

package notification_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// SendNotificationServiceClient is the client API for SendNotificationService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type SendNotificationServiceClient interface {
	SendToOne(ctx context.Context, in *SendNotificationToOne, opts ...grpc.CallOption) (*emptypb.Empty, error)
	SendToAll(ctx context.Context, in *SendNotificationToAll, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type sendNotificationServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewSendNotificationServiceClient(cc grpc.ClientConnInterface) SendNotificationServiceClient {
	return &sendNotificationServiceClient{cc}
}

func (c *sendNotificationServiceClient) SendToOne(ctx context.Context, in *SendNotificationToOne, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/genproto.SendNotificationService/SendToOne", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *sendNotificationServiceClient) SendToAll(ctx context.Context, in *SendNotificationToAll, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/genproto.SendNotificationService/SendToAll", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// SendNotificationServiceServer is the server API for SendNotificationService service.
// All implementations must embed UnimplementedSendNotificationServiceServer
// for forward compatibility
type SendNotificationServiceServer interface {
	SendToOne(context.Context, *SendNotificationToOne) (*emptypb.Empty, error)
	SendToAll(context.Context, *SendNotificationToAll) (*emptypb.Empty, error)
	mustEmbedUnimplementedSendNotificationServiceServer()
}

// UnimplementedSendNotificationServiceServer must be embedded to have forward compatible implementations.
type UnimplementedSendNotificationServiceServer struct {
}

func (UnimplementedSendNotificationServiceServer) SendToOne(context.Context, *SendNotificationToOne) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendToOne not implemented")
}
func (UnimplementedSendNotificationServiceServer) SendToAll(context.Context, *SendNotificationToAll) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendToAll not implemented")
}
func (UnimplementedSendNotificationServiceServer) mustEmbedUnimplementedSendNotificationServiceServer() {
}

// UnsafeSendNotificationServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to SendNotificationServiceServer will
// result in compilation errors.
type UnsafeSendNotificationServiceServer interface {
	mustEmbedUnimplementedSendNotificationServiceServer()
}

func RegisterSendNotificationServiceServer(s grpc.ServiceRegistrar, srv SendNotificationServiceServer) {
	s.RegisterService(&SendNotificationService_ServiceDesc, srv)
}

func _SendNotificationService_SendToOne_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SendNotificationToOne)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SendNotificationServiceServer).SendToOne(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/genproto.SendNotificationService/SendToOne",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SendNotificationServiceServer).SendToOne(ctx, req.(*SendNotificationToOne))
	}
	return interceptor(ctx, in, info, handler)
}

func _SendNotificationService_SendToAll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SendNotificationToAll)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SendNotificationServiceServer).SendToAll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/genproto.SendNotificationService/SendToAll",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SendNotificationServiceServer).SendToAll(ctx, req.(*SendNotificationToAll))
	}
	return interceptor(ctx, in, info, handler)
}

// SendNotificationService_ServiceDesc is the grpc.ServiceDesc for SendNotificationService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var SendNotificationService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "genproto.SendNotificationService",
	HandlerType: (*SendNotificationServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SendToOne",
			Handler:    _SendNotificationService_SendToOne_Handler,
		},
		{
			MethodName: "SendToAll",
			Handler:    _SendNotificationService_SendToAll_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "send_notification_service.proto",
}
