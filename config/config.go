package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

var (
	CacheTTL = "3 minutes"
)

// Config ...
type Config struct {
	Environment string // develop, staging, production

	LogLevel string
	RPCPort  string
	SDN      string

	CourierServiceHost string
	CourierServicePort string

	UserServiceHost string
	UserServicePort string

	SmsServiceHost string
	SmsServicePort string

	PostgresHost     string
	PostgresPort     string
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string
	MaxConnections   int
}

// Load loads environment vars and inflates Config
func Load() Config {
	if err := godotenv.Load("/app/.env"); err != nil {
		fmt.Println("No .env file found")
	}

	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))

	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.RPCPort = cast.ToString(getOrReturnDefault("RPC_PORT", ":9100"))

	c.CourierServiceHost = cast.ToString(getOrReturnDefault("COURIER_SERVICE_HOST", "localhost"))
	c.CourierServicePort = cast.ToString(getOrReturnDefault("COURIER_GRPC_PORT", "80"))

	c.UserServiceHost = cast.ToString(getOrReturnDefault("USER_SERVICE_HOST", "localhost"))
	c.UserServicePort = cast.ToString(getOrReturnDefault("USER_GRPC_PORT", "9001"))

	c.SmsServiceHost = cast.ToString(getOrReturnDefault("SMS_SERVICE_HOST", "sms-service"))
	c.SmsServicePort = cast.ToString(getOrReturnDefault("SMS_GRPC_PORT", "80"))

	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToString(getOrReturnDefault("POSTGRES_PORT", "5432"))

	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "auth_service"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "postgres"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "postgres"))
	c.MaxConnections = cast.ToInt(getOrReturnDefault("MAX_CONNECTIONS", 5))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	if os.Getenv(key) == "" {
		return defaultValue
	}
	return os.Getenv(key)
}
