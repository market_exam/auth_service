package main

import (
	"context"

	"app/config"
	"app/grpc"
	"app/grpc/grpc_client"
	"app/pkg/logger"
	"app/storage/postgres"

	"net"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "auth_service")
	defer logger.Cleanup(log)

	pgStore, err := postgres.NewPostgres(context.Background(), cfg, log)
	if err != nil {
		log.Error("postgres.NewPostgres", logger.Error(err))
	}
	defer pgStore.CloseDB()

	clients, err := grpc_client.New(cfg)
	if err != nil {
		log.Error("error while connecting other services", logger.Error(err))
		return
	}
	grpcServer := grpc.SetUpServer(cfg, log, pgStore, clients)

	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Error("error while listening: %v", logger.Error(err))
		return
	}

	log.Info("main: server running",
		logger.String("port", cfg.RPCPort))

	if err := grpcServer.Serve(lis); err != nil {
		log.Error("error while listening: %v", logger.Error(err))
	}
}
